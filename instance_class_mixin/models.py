
from django.db import models
from instance_class_mixin.functions import get_mixin, mixin


class ClassMixin(models.Model):
    
    '''
        store the python path relative to MIXIN_PATH
    '''
    mixin_class_path = models.CharField(
        max_length = 128, 
        blank = True, 
        null = True, 
        default = None
    )
    
    '''
        the directory where mixins are stored as python path, e.g. code.mixins;
        should probably be different for each ClassMixin-derived model 
    '''
    MIXIN_PATH = 'mixins'
    
    @property
    def mixin_class(self):
        if self.mixin_class_path is None:
            return None
        if self.MIXIN_PATH:
            return get_mixin('%s.%s' % (self.MIXIN_PATH, self.mixin_class_path))
        return get_mixin(self.mixin_class_path)
    
    '''
        extend init to mix the class in mixin_class_file into
        this instance, granting it the methods from that class
    '''
    def __init__(self, *args, **kwargs):
        super(ClassMixin, self).__init__(*args, **kwargs)
        if self.mixin_class is not None:
            mixin(self, self.mixin_class)
    
    class Meta:
        abstract = True


