
from inspect import isclass
from imp import load_source
from os.path import abspath
from importlib import import_module
from inspect import getmembers, isclass


class MixinNotFoundError(Exception):
    ''' mixin class file not found, or no suitable class inside file '''


def get_mixin(py_path, inst = None):
    module_path, cls_name = py_path.rsplit('.', 1)
    err_msg = ''
    if inst:
        err_msg = ' (it is supposed to contain a mixin for instance %s)' % inst
    try:
        modul = import_module(module_path)
    except ImportError:
        raise MixinNotFoundError('path "%s" could not be imported while looking for mixin%s' % (module_path, err_msg))
    try:
        cls = getattr(modul, cls_name)
    except AttributeError:
        raise MixinNotFoundError('path "%s" imported succesfully, but doesn\'t contain %s%s' % (module_path, cls_name, err_msg))
    if not isclass(cls):
        raise MixinNotFoundError('%s from %s is not a class%s' % (cls_name, module_path, err_msg))
    return cls


'''
    get classes in a module by python path (e.g. code.stuff)
    note to self: do not use __import__; it doesn't work but import_module does
'''
def classes_in_module(module_pypath):
    modul = import_module(module_pypath)
    return [obj for name, obj in getmembers(modul) if isclass(obj)]


'''
    takes an instance and mixes cls into it's bases, giving it
    all the properties of class (as far as they don't collide)
'''
def mixin(inst, cls):
    ''' keep the class name or django will get the table name wrong '''
    name = inst.__class__.__name__
    attrs = {'__module__': inst.__class__.__module__}
    bases = inst.__class__.__bases__ + (cls, )
    inst.__class__ = type(name, bases, attrs)


