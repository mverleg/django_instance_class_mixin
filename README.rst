
Django Instance Class Mixin
-----------

If you have ever wanted to have different methods for different instances of the same model, then this little module is the solution (or a solution, really).

You can define several classes with methods you would like some instances to have. Then you can indicate for each instance from which class it should obtain all methods.

An example is given in this codereview stackexchange question: http://codereview.stackexchange.com/questions/46907/django-models-different-methods-on-each-instance

Installation & Configuration:
-----------

- Install using ``pip install git+https://bitbucket.org/mverleg/django_instance_class_mixin.git`` (or download and copy the app into your project). 

You do not need to add anything to your installed apps.

Usage
-----------

All you need to do to create a mixin model is this:

                from instance_class_mixin import ClassMixin
                class MonsterClass(ClassMixin):
                    MIXIN_DIR = '/path/to/your/mixins/for/this/model'

This will add an (optional) field to the model to specify a relative path to a file containing a class. If specified, this file is added to the bases for this instance (leaving other instances unaffected). You can then use the methods on this class for this instance.

For example:

                # models.py
                class MonsterClass(ClassMixin):
                    MIXIN_DIR = PATH_TO_MIXIN_FILES
                    name = models.CharField(max_length = 32)
                    attack = models.PositiveIntegerField(default = 1)
                    defense = models.PositiveIntegerField(default = 10)
                    MIXIN_DIR = PATH_TO_MIXIN_FILES
    
                class MonsterInstance(models.Model):
                    cls = models.ForeignKey(MonsterClass)
                    x = models.FloatField()
                    y = models.FloatField()

This allows you to create a file to give specific abilities to each type of monster, like this:

                # slimy_monster.py
                class SlimyMonster(): # not a Model
                    def use_unique_ability(self, target):
                        target.defense -= target.attack * 2
                    def check_attack(self, player): # should I attack this player?
                        return target.defense * target.attack < self.defense * self.attack * 1.5

In most any case, you'll want to give each instance the same methods, but with different implementation (each monster has a special ability, but what they do differs). Then just create it like this:

                slimy = MonsterClass(attack = 3, defense = 12, name = 'Slimy', mixin_class_file = 'slimy_monster.py')

After which this slimy can do ``slimy.use_unique_ability(self, target)``, which will be different from ``bunny.use_unique_ability(self, target)``, which would use another mixin file.

License
-----------

django_displayable is available under the revised BSD license, see LICENSE.txt. You can do anything as long as you include the license, don't use my name for promotion and are aware that there is no warranty.


